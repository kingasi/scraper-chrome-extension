console.log('content.js running');

chrome.runtime.onMessage.addListener(gotMessage);

let classList = {};
let outline = "2px solid #a13";

function gotMessage (message, sender, sendResponse) {
    console.log('received msg',message);
    
    if(message.type === 'start_selecting'){
        startScraping();
    }
    else if(message.type === 'stop_selecting'){
        stopScraping();
    }
    else if(message.type === 'get_site_url'){
        sendSiteUrl();
    }
    else if(message.type === 'scraper_object_error'){
        alert(message.value)
    }
    else if(message.type === 'object_sent'){
        clearOutlines();
        alert(message.value)
    }
}

function sendSiteUrl() {
    let message = {
        type: 'site_url',
        value: window.location.href
    }
    
    console.log('message to send',message);
    chrome.runtime.sendMessage(message);
}

// function applyMask(target) {
//     if(document.getElementsByClassName('highlight-wrap').length > 0) {
//         resizeMask(target);
//     }else{
//         createMask(target);
//     }
// }

// function resizeMask(target) {
//     var rect = target.getBoundingClientRect();
//     var hObj = document.getElementsByClassName('highlight-wrap')[0];
//     hObj.style.top=rect.top+"px";
//     hObj.style.width=rect.width+"px";
//     hObj.style.height=rect.height+"px";
//     hObj.style.left=rect.left+"px";
//    // hObj.style.WebkitTransition='top 0.2s';
// }

// function createMask(target) {
//     var rect = target.getBoundingClientRect();
//     var hObj = document.createElement("div");
//     hObj.className = 'highlight-wrap';
//     hObj.style.position='absolute';
//     hObj.style.top=rect.top+"px";
//     hObj.style.width=rect.width+"px";
//     hObj.style.height=rect.height+"px";
//     hObj.style.left=rect.left+"px";
//     hObj.style.backgroundColor = '#205081';
//     hObj.style.opacity='0.5';
//     hObj.style.cursor='default';
//     hObj.style.pointerEvents='none';
//     //hObj.style.WebkitTransition='top 0.2s';
//     document.body.appendChild(hObj);
// }

// function clearMasks() {
//     var hwrappersLength = document.getElementsByClassName("highlight-wrap").length;
//     var hwrappers = document.getElementsByClassName("highlight-wrap");
//     if(hwrappersLength > 0) {
//         for(var i=0; i<hwrappersLength; i++) {
//             console.log("Removing existing wrap");
//             hwrappers[i].remove();
//         }
//     }
// }

function startScraping() {
    window.addEventListener('click', selectItem);
    
    window.addEventListener('mouseover', mouseOver);
}
function selectItem(e) {
    e.preventDefault();
    processEvent(e.target);
    return false;
}
function mouseOver(e) {
    let target = e.target;
    
    const oldOutlineStyle = target.style.outline;

    if(oldOutlineStyle.indexOf('solid') == -1){
        target.addEventListener("mouseenter", function( event ) {         
            target.style.outline = outline;                
        }, false);  
        
        target.addEventListener("mouseleave", function( event ) {
            if(target.style.outline.indexOf('dotted') === -1){
                target.style.outline = oldOutlineStyle;
            }
        }, false);
    }
    else {
        target.style.outline = "";
    }  
}
function processEvent(element) {
    const value = element.classList.value;
    console.log('class list',value);
    
    if(value){
        if(classList[value]){
            element.style.outline = "";
            console.log('value already existing',value);
            delete classList[value]
        }
        else {
            element.style.outline = "3px dotted #a13";
            classList[value] = value;
        }

        chrome.runtime.sendMessage({
            type: 'add_to_child_object',
            key: 'selector',
            value: Object.keys(classList).map(cls=> `.${cls}`).toString()
        });
    }
}

function clearOutlines() {
    if(Object.keys(classList).length > 0){
        Object.keys(classList).forEach(cls => {
            document.getElementsByClassName(cls)[0].style.outline = "";
        })
    }
}

function stopScraping() {
    outline = "";
    document.removeEventListener('click',selectItem)
    document.removeEventListener('mouseover', mouseOver);
    console.log('stop_selecting');
}