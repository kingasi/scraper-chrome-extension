function setup() {
    chrome.runtime.sendMessage({type: 'get_scraper_object'});

    chrome.runtime.onMessage.addListener(receivedMessage);
    let params = {
        active: true,
        currentWindow: true
    }

    let active = false;

    let powerBtn = document.getElementById("powerBtn");
    let doneBtn = document.getElementById("done-btn");
    let scrapeSelfCheckBox = document.getElementById("scrape-self");
    let email = document.getElementById("email-address");
    let url = document.getElementById("url");
    let name = document.getElementById("main-name-input");
    let type = document.getElementById("main-select-type");
    let childEnableList = document.getElementById("main-enable-list");
    let childSelectors = document.getElementById("main-selectors");

    powerBtn.addEventListener("click", powerBtnClicked);
    doneBtn.addEventListener("click", doneBtnClicked)
    childSelectors.addEventListener("change", childSelectorsChanged)
    childEnableList.addEventListener("change", changeChildEnableList)
    url.addEventListener("change", urlChange);
    scrapeSelfCheckBox.addEventListener("click", getSiteUrl);
    email.addEventListener("change", emailChange);
    name.addEventListener("change", mainNameChange);
    type.addEventListener("change", mainTypeChange);
    document.getElementById("main-start-selecting").addEventListener("click", startSelecting);
    document.getElementById("add-child-main-btn").addEventListener("click", addObject)
    
    function powerBtnClicked() {
        powerBtn.checked = !active;
        chrome.runtime.sendMessage({type: 'toggle-power-button'});
        if(active){
            chrome.tabs.query(params, (tabs) => {
                let msg = {
                    type: 'stop_selecting',
                }
        
                chrome.tabs.sendMessage(tabs[0].id, msg);
            });
        }

        active = !active;
    }
    function doneBtnClicked() {
        chrome.runtime.sendMessage({type: 'process_and_send_job'});
    }

    function receivedMessage (message, sender, sendResponse) {
        console.log('pop up receivedMessage msg',message);
        url = document.getElementById("url");
        name = document.getElementById("main-name-input");

        scrapeSelfCheckBox = document.getElementById("scrape-self");

        if(message.type === 'site_url'){
            console.log('url now is',url);
            
            url.value = message.value;
            chrome.runtime.sendMessage({
                type: 'add_url',
                value: message.value
            });
        }
        else if(message.type === 'scraper_object') {
            const scraper_object = message.value;
            active = message.active;
            powerBtn.checked = active;
            console.log('scraper_object',scraper_object);

            url.value = scraper_object.scrape[0].url;
            email.value = scraper_object.scrape[0].callback.payload.mailingList.toString();
            
            if(scraper_object['childObject']){
                scrapeSelfCheckBox.checked = scraper_object.childObject.url ? true : false;
                name.value = scraper_object.childObject.name || '';
                type.value = scraper_object.childObject.type || 'response';
                childEnableList.checked = scraper_object.childObject.list;
                childSelectors.value = scraper_object.childObject.selector;

                if(scraper_object.childObject.children && scraper_object.childObject.children.length > 0){
                    const data = scraper_object.childObject.children[0];
                    document.getElementById("add-child-main-btn").click();
                    setInnerChildValues(data)
                }
            }
        }
        else if(message.type === 'scraper_object_error'){
            chrome.tabs.query(params, (tabs) => {
                let msg = {
                    type: message.type,
                    value: message.value
                }
        
                chrome.tabs.sendMessage(tabs[0].id, msg);
            });
        }
        else if(message.type === 'object_sent'){
            chrome.tabs.query(params, (tabs) => {
                let msg = {
                    type: message.type,
                    value: message.value
                }
        
                chrome.tabs.sendMessage(tabs[0].id, msg);
            });

            window.close();
        }
    }

    function setInnerChildValues(data) {
        document.getElementById('child-name-input').value = data.name;
        document.getElementById('child-select-type').value = data.type;
        document.getElementById('child-enable-list').checked = data.list;
        document.getElementById('child-selector').value = data.selector;
    }

    function getSiteUrl() {
        if(scrapeSelfCheckBox.checked == true){
            chrome.tabs.query(params, (tabs) => {
                let message = {
                    type: 'get_site_url'
                }
        
                chrome.tabs.sendMessage(tabs[0].id, message);
            });
        }
        else {
            url.value = "";
            chrome.runtime.sendMessage({
                type: 'add_url',
                key: 'url',
                value: ""
            });
        }
    }

    function mainNameChange() {        
        chrome.runtime.sendMessage({
            type: 'add_to_child_object',
            key: 'name',
            value: name.value
        });
    }

    function childSelectorsChanged() {
        chrome.runtime.sendMessage({
            type: 'add_to_child_object',
            key: 'selector',
            value: childSelectors.value
        });
    }

    function changeChildEnableList() {
        chrome.runtime.sendMessage({
            type: 'add_to_child_object',
            key: 'list',
            value: childEnableList.checked
        });
    }

    function emailChange() {
        let temp = email.value.replace(' ','');
        temp = temp.split(',')
        chrome.runtime.sendMessage({
            type: 'add_email_address',
            value: temp
        });
    }

    function urlChange() {
        chrome.runtime.sendMessage({
            type: 'add_url',
            value: url.value
        });
    }
    function mainTypeChange() {
        console.log('type.value',type.value);
        
        chrome.runtime.sendMessage({
            type: 'add_to_child_object',
            key: 'type',
            value: type.value
        });
    }

    function startSelecting() {
        console.log('startSelecting called');
    
        chrome.tabs.query(params, (tabs) => {
            let message = {
                type: 'start_selecting'
            }
    
            chrome.tabs.sendMessage(tabs[0].id, message);
        });

        chrome.runtime.sendMessage({type: 'toggle-power-button'});
        window.close();
    }

    function addObject() {
        const templateScrape = document.getElementById("template");
        const newObject = templateScrape.cloneNode(true);
        const children = document.getElementById("main-children");
        children.append(newObject);

        this.parentNode.remove();
        const elements = document.getElementsByClassName('template-object');
        if(elements[1]){
            elements[1].setAttribute('style','width: 90%; float: right; margin-bottom: 10px;');
            elements[0].innerHTML = '<div></div>'
        }

        document.getElementById('child-name-input').addEventListener("change", childNameChange);
        document.getElementById('child-select-type').addEventListener("change", childTypeChange);
        document.getElementById('child-enable-list').addEventListener("change", childListChange);
        document.getElementById('child-selector').addEventListener("change", childSelectorChange);
    }

    function childNameChange(event) {
        chrome.runtime.sendMessage({
            type: 'add_to_inner_child_object',
            key: 'name',
            value: event.target.value
        })
    }

    function childTypeChange(event) {
        chrome.runtime.sendMessage({
            type: 'add_to_inner_child_object',
            key: 'type',
            value: event.target.value
        })
    }

    function childListChange(event) {
        chrome.runtime.sendMessage({
            type: 'add_to_inner_child_object',
            key: 'list',
            value: event.target.checked
        })
    }

    function childSelectorChange() {
        chrome.runtime.sendMessage({
            type: 'add_to_inner_child_object',
            key: 'selector',
            value: event.target.value
        })
    }
}

setup();