console.log('background running');

chrome.runtime.onMessage.addListener(gotMessage);

const scrape = [{
    name:"",
    url:"",
    method: "GET",
    headers: {},
    type: "response",
    selector: "",
    list: false,
    proxy: false,
    escaping: false,
    children: [],
    callback: {
        active: false,
        url: "", //TODO. specify url
        headers: {},
        payload: {
            mailingList: []
        }
    }
}];

let active = false;

const childObject = {
    name: "",
    type: "response",
    selector: "",
    list: false,
    children: []
};

const innerChildObject = {
    name: "",
    type: "response",
    selector: "",
    list: false,
    children: []
};


function gotMessage (message, sender, sendResponse) {
    console.log('background received msg',message);
    
    if(message.type === 'add_email_address'){
        scrape[0].callback.payload.mailingList = message.value;
    }
    else if(message.type === 'add_url'){
        scrape[0].url = message.value;
    }
    else if(message.type === 'add_to_child_object'){
        childObject[message.key] = message.value;

        console.log('childObject',childObject);
    }
    else if(message.type === 'add_to_inner_child_object'){
        innerChildObject[message.key] = message.value;

        childObject.children[0] = innerChildObject;
        console.log('innerChildObject',innerChildObject);
    }
    else if(message.type === 'change_main_type'){
        scrape[0].type = message.value;

        console.log('current scrape',scrape);
    }
    else if(message.type === 'get_scraper_object'){
        console.log('scrape',scrape,'childObject',childObject);
        
        chrome.runtime.sendMessage({
            type: 'scraper_object',
            value: { scrape, childObject },
            active
        });
    }
    else if(message.type === 'process_and_send_job'){
        console.log('process and send job');

        const data = scrape[0];
        if(data.url === ""){
            chrome.runtime.sendMessage({
                type: 'scraper_object_error',
                value: 'What is the url you want to scrape?',
            });
        }
        else if(data.callback.payload.mailingList.length == 0){
            chrome.runtime.sendMessage({
                type: 'scraper_object_error',
                value: 'At least an email is required',
            });
        }
        else if(childObject.name === ""){
            chrome.runtime.sendMessage({
                type: 'scraper_object_error',
                value: 'Add item name',
            });
        }
        else if(childObject.selector === ""){
            chrome.runtime.sendMessage({
                type: 'scraper_object_error',
                value: 'You have no selectors added',
            });
        }
        else if(childObject.children.length > 0){
            const innerChild = childObject.children[0];

            if(innerChild.name === ""){
                chrome.runtime.sendMessage({
                    type: 'scraper_object_error',
                    value: 'Your inner item has no name specified',
                });
            }
            else if(innerChild.selector === ""){
                chrome.runtime.sendMessage({
                    type: 'scraper_object_error',
                    value: 'Selectors required for the inner item',
                });
            }
        }
        else {
            scrape[0].children = [childObject];
            console.log('final scrape object',scrape);
            
            const endpoint = '';
            fetch(endpoint, {
                method: 'POST', 
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(scrape)
            })
            .then(response => {
                chrome.runtime.sendMessage({
                    type: 'object_sent',
                    value: 'Your request has been sent successfully!',
                });
            })
            .catch(error => {
                chrome.runtime.sendMessage({
                    type: 'scraper_object_error',
                    value: error.message,
                });
            })
            
        }
    }
    else if(message.type === 'toggle-power-button'){
        active = !active;
        console.log('active state is',active);
    }
}
